package grafika;

import com.jogamp.opengl.util.FPSAnimator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Hashtable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import rasterization.Renderer;

/**
 * trida pro kresleni na platno:
 * zobrazeni  pixelu 
 * @author PGRF FIM UHK
 * @version 2015
 */

public final class Canvas {

	private final Controller controller;
	private final JFrame frame;
	private final JPanel panel_left;
	private final JPanel panel_right;

	private Renderer renderer;

	public Canvas(int width, int height) {
		BorderLayout layout = new BorderLayout();
		
		controller = new Controller(16, 16);
		
		frame = new JFrame();
		frame.setTitle("PGR2:RICHTLU:Semestrální práce");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		JLabel popis_label = new JLabel();
		popis_label.setText("<html><body><u>Lukáš Richtrmoc</u><br>PGR2, 13.5.2017/31.7.2017<br><br><u>Prohlížení</u><br>Levé tlačítko - rozhlížení<br>Kolečko up/down - přiblížení<br>R - reset<br>T - axis on/off<br>V - wire<br><br><u>Úpravy</u> (podle os)<br>Pohyb mraku - I,J,K,L<br>Výška mraku - U/O<br>Pohyb větru - W,A,S,D<br><br><br><u>SPEED</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>QUANTITY</u></body></html>");
		popis_label.setForeground(Color.WHITE);
//		popis_label.setBorder(BorderFactory.createEmptyBorder(20,0,0,0));
		
		JSlider quantitySnowing = new JSlider(JSlider.VERTICAL,0, 4, 0);
		quantitySnowing.setPreferredSize(new Dimension(80, 120));
		Hashtable quantityLabel = new Hashtable();
		quantityLabel.put( 0, new JLabel("0") );
		quantityLabel.put( 1, new JLabel("1") );
		quantityLabel.put( 2, new JLabel("2") );
		quantityLabel.put( 3, new JLabel("3") );
		quantityLabel.put( 4, new JLabel("4") );
		quantityLabel.forEach((k,v)->{
			JLabel label = (JLabel)v;
			label.setForeground(Color.WHITE);
		});
		quantitySnowing.setLabelTable(quantityLabel);
		quantitySnowing.setMajorTickSpacing(40);
		quantitySnowing.setPaintTicks(true);
		quantitySnowing.setPaintLabels(true);
		quantitySnowing.setOpaque(false);
		controller.setSliderQuantity(quantitySnowing);
		
		JSlider speedSnowing = new JSlider(JSlider.VERTICAL,0, 25, 5);
		speedSnowing.setPreferredSize(new Dimension(80, 120));
		Hashtable snowingLabel = new Hashtable();
		snowingLabel.put( 0, new JLabel("STOP") );
		snowingLabel.put( 5, new JLabel("NORMAL") );
		snowingLabel.put( 25, new JLabel("MAX") );
		snowingLabel.forEach((k,v)->{
			JLabel label = (JLabel)v;
			label.setForeground(Color.WHITE);
		});
		speedSnowing.setLabelTable( snowingLabel );
		speedSnowing.setMajorTickSpacing(40);
		speedSnowing.setPaintTicks(false);
		speedSnowing.setPaintLabels(true);
		speedSnowing.setOpaque(false);
		controller.setSliderSpeed(speedSnowing);
		
		JLabel temperature_label = new JLabel();
		temperature_label.setText("<html><body><u>TEMPERATURE</u></body></html>");
		temperature_label.setForeground(Color.WHITE);
		temperature_label.setBorder(BorderFactory.createEmptyBorder(20,0,0,0));
		
		JSlider temperature = new JSlider(JSlider.HORIZONTAL,-4, 4, 0);
		temperature.setPreferredSize(new Dimension(160, 80));
		Hashtable temperatureLabel = new Hashtable();
		temperatureLabel.put( -4, new JLabel("-4") );
		temperatureLabel.put( 0, new JLabel("0") );
		temperatureLabel.put( 4, new JLabel("4") );
		temperatureLabel.forEach((k,v)->{
			JLabel label = (JLabel)v;
			label.setForeground(Color.WHITE);
		});
		temperature.setLabelTable(temperatureLabel);
		temperature.setMajorTickSpacing(40);
		temperature.setPaintTicks(true);
		temperature.setPaintLabels(true);
		temperature.setOpaque(false);
		controller.setSliderTemperature(temperature);
		
		JLabel cloudsize_label = new JLabel();
		cloudsize_label.setText("<html><body><u>CLOUD SIZE</u>&nbsp;&nbsp;</body></html>");
		cloudsize_label.setForeground(Color.white);
		JSpinner cloudsize = new JSpinner(new SpinnerNumberModel(Math.round(controller.getWidth()*.8f), Math.round(controller.getWidth()/3), Math.round(controller.getWidth() * 1.25f), 1));
		controller.setSpinnerCloud(cloudsize);
		
		
		panel_left = new JPanel();
		panel_left.setPreferredSize(new Dimension(180, height));
		panel_left.setBackground(Color.DARK_GRAY);
		panel_left.setBorder(BorderFactory.createEmptyBorder(15,0,0,0));
		panel_left.add(popis_label);
		panel_left.add(speedSnowing);
		panel_left.add(quantitySnowing);
		panel_left.add(temperature_label);
		panel_left.add(temperature);
		panel_left.add(cloudsize_label);
		panel_left.add(cloudsize);

		renderer = new Renderer(controller);

		panel_right = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panel_right.setPreferredSize(new Dimension(width, height));
		
		// OPEN GL START
		
		//getting the capabilities object of GL2 profile
		final GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);
		capabilities.setRedBits(8);
		capabilities.setBlueBits(8);
		capabilities.setGreenBits(8);
		capabilities.setAlphaBits(8);
		capabilities.setDepthBits(24);

		final GLCanvas canvas = new GLCanvas(capabilities);
		canvas.setSize(width, height);
		canvas.setBackground(Color.red);
		canvas.addGLEventListener(renderer);
		canvas.addKeyListener(renderer);
		canvas.addMouseListener(renderer);
		canvas.addMouseMotionListener(renderer);
		canvas.addMouseWheelListener(renderer);
		
		panel_right.add(canvas);
		final FPSAnimator animator = new FPSAnimator(canvas, 60, true);
		animator.start();
		
		layout.addLayoutComponent(panel_left, BorderLayout.WEST);
		layout.addLayoutComponent(panel_right, BorderLayout.EAST);
		
		frame.add(panel_left);
		frame.add(panel_right);
		frame.setLayout(layout);
		frame.pack();
		frame.setVisible(true);
		
		panel_right.setFocusable(true);
		panel_right.requestFocusInWindow();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new Canvas(800, 600));
	}

}