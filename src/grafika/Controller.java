/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafika;

import javax.swing.JSlider;
import javax.swing.JSpinner;

/**
 *
 * @author lukas.richtrmoc
 */
public class Controller {
	private boolean mouseMoving;
	private int mouseClicked;
	
	private final int width, height;
	
	private JSlider sliderSpeed, sliderQuantity, sliderTemperature;
	private JSpinner spinnerCloud;
	

	public Controller(int width, int height) {
		mouseMoving = true;
		mouseClicked = 0;
		
		this.width = width;
		this.height = height;
	}

	public boolean isMouseMoving() {
		return mouseMoving;
	}

	public void setMouseMoving(boolean mouseMoving) {
		this.mouseMoving = mouseMoving;
	}

	public int getMouseClicked() {
		return mouseClicked;
	}
	
	public void addMouseClick(){
		mouseClicked++;
	}

	public void setSliderSpeed(JSlider sliderSpeed) {
		this.sliderSpeed = sliderSpeed;
	}

	public void setSliderQuantity(JSlider sliderQuantity) {
		this.sliderQuantity = sliderQuantity;
	}
	
	public JSlider getSliderSpeed() {
		return sliderSpeed;
	}

	public JSlider getSliderQuantity() {
		return sliderQuantity;
	}

	public JSlider getSliderTemperature() {
		return sliderTemperature;
	}

	public void setSliderTemperature(JSlider sliderTemperature) {
		this.sliderTemperature = sliderTemperature;
	}

	public JSpinner getSpinnerCloud() {
		return spinnerCloud;
	}

	public void setSpinnerCloud(JSpinner spinnerCloud) {
		this.spinnerCloud = spinnerCloud;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
