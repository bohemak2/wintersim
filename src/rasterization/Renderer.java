/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasterization;

import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import grafika.Controller;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.glu.GLU;
import javax.swing.SwingUtilities;


/**
 *	
 * @author lukas.richtrmoc
 */
public class Renderer implements GLEventListener, KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {
	private Snowing snowing;
	private Controller controller;
	Texture texture_grass, texture_snow, texture_ground, texture_tree, texture_bush;
	private float dx, dy;
	private double azimut = -130, zenit = 30;
	private float startX = 0, startY = 0, windX = 0, windY = 0, zoom;
	private int tempCloud;
	private boolean wire = false, axis = false;
	private GLU glu;
	private GLUT glut;
	
	private Board board;
	
	public Renderer(Controller controller) {
		this.controller = controller;
		this.board = new Board(controller.getWidth(), controller.getHeight());
		this.snowing = new Snowing(board, controller);
		this.zoom = -snowing.getCloud().getZ() * 1.75f;
		tempCloud = (Integer)controller.getSpinnerCloud().getValue();
	}


	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		try {
			texture_grass = TextureIO.newTexture(getClass().getResourceAsStream("/grass.jpg"), true, "jpg");
			texture_ground = TextureIO.newTexture(getClass().getResourceAsStream("/ground.jpg"), true, "jpg");
			texture_snow = TextureIO.newTexture(getClass().getResourceAsStream("/snow.jpg"), true, "jpg");
			texture_tree = TextureIO.newTexture(getClass().getResourceAsStream("/tree.png"), true, "png");
//			texture_bush = TextureIO.newTexture(getClass().getResourceAsStream("/bush.png"), true, "png");
		} catch (GLException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		glu = new GLU();
		glut = new GLUT();
		
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glClearColor(0f, 0f, 0f, 1f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();

		glu.gluPerspective(60f, controller.getWidth() /  controller.getHeight(), 0.1f, 100.0f);
		
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		
		zenit += dy;
		dy = 0;
		if (zenit > 90)
			zenit = 90;
		if (zenit < -90)
			zenit = -90;
		azimut += dx;
		dx = 0;
		azimut = azimut % 360;
		gl.glTranslatef(0f, 0f, zoom);
		gl.glRotated(zenit - 90, 1, 0, 0);
		gl.glRotated(azimut, 0, 0, 1);
		gl.glTranslatef(- board.getWidth() / 2, - board.getHeight() / 2, -2);
		
		if (wire){
			gl.glPolygonMode(GL2.GL_FRONT_AND_BACK,GL2.GL_LINE);
		}else{
			gl.glPolygonMode(GL2.GL_FRONT_AND_BACK,GL2.GL_FILL);
		}
		/* TEST svetla*/
//		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float[] { board.getWidth(), board.getHeight(), snowing.getCloudZ() * 1.5f, 1f }, 0);
//		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPOT_DIRECTION, new float[] {0,0,0,0.0f},0);
//		gl.glLightf(GL2.GL_LIGHT0, GL2.GL_SPOT_CUTOFF,180);
//		gl.glShadeModel(GL2.GL_SMOOTH);
//		
//		gl.glEnable(GL2.GL_LIGHTING);
//		gl.glEnable(GL2.GL_LIGHT0);
		
		
		gl.glLineWidth(2);
		if(axis){
			gl.glBegin(GL2.GL_LINES); // X - red
				gl.glColor3f(1.0f, .0f, 0.0f);
				gl.glVertex3f(0.0f, 0.0f, 0.0f);
				gl.glColor3f(1.0f, .0f, 0.0f);
				gl.glVertex3f(15.0f, 0.0f, 0.0f);
			gl.glEnd();
			gl.glBegin(GL2.GL_LINES); // Y - green
				gl.glColor3f(0.0f, 1.0f, 0.0f);
				gl.glVertex3f(0.0f, 0.0f, 0.0f);
				gl.glColor3f(0.0f, 1.0f, 0.0f);
				gl.glVertex3f(0.0f, 15.0f, 0.0f);
			gl.glEnd();
			gl.glBegin(GL2.GL_LINES); // Z - blue
				gl.glColor3f(0.0f, 0.0f, 1.0f);
				gl.glVertex3f(0.0f, 0.0f, 0.0f);
				gl.glColor3f(0.0f, 0.0f, 1.0f);
				gl.glVertex3f(0.0f, 0.0f, 15.0f);
			gl.glEnd();
		}
		
		
		snowing.refreshFlakes();
		gl.glEnable( GL2.GL_POINT_SMOOTH );
		gl.glEnable( GL2.GL_POINT_SPRITE );
		
		snowing.getFlakes().forEach((SnowFlake flake) -> {
			gl.glPointSize(flake.getSize());
			gl.glBegin(GL2.GL_POINTS);
			gl.glColor3f(1.0f, 1.0f, 1.0f);
			gl.glVertex3f(flake.getX(), flake.getY(), flake.getZ());
			gl.glEnd();
		});

		gl.glBegin(GL2.GL_QUADS);
			gl.glColor3f(0f, 0f, 0.f);
			gl.glVertex3f(0.0f, 0.0f, -5.0f);
			gl.glColor3f(0f, 0f, 0.f);
			gl.glVertex3f(board.getWidth(), 0.0f, -5.0f);
			gl.glColor3f(0f, 0f, 0.f);
			gl.glVertex3f(board.getWidth(), board.getHeight(), -5.0f);
			gl.glColor3f(0f, 0f, 0.f);
			gl.glVertex3f(0.0f, board.getHeight(), -5.0f);
		gl.glEnd();
		
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_ADD);
		
		texture_grass.bind(gl);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0f, 0f);
			gl.glVertex3f(0.0f, 0.0f, 0.0f);
			gl.glTexCoord2f(0f, 5f);
			gl.glVertex3f(board.getWidth(), 0.0f, 0.0f);
			gl.glTexCoord2f(5f, 5f);
			gl.glVertex3f(board.getWidth(), board.getHeight(), 0.0f);
			gl.glTexCoord2f(5f, 0f);
			gl.glVertex3f(0.0f, board.getHeight(), 0.0f);
		gl.glEnd();

		
		texture_ground.bind(gl);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0f, 1f);
			gl.glVertex3f(0.0f, 0.0f, 0.0f);
			gl.glTexCoord2f(0f, 0f);
			gl.glVertex3f(0.0f, 0.0f, -5.0f);
			gl.glTexCoord2f(10f, 0f);
			gl.glVertex3f(board.getWidth(),0.0f, -5.0f);
			gl.glTexCoord2f(10f, 1f);
			gl.glVertex3f(board.getWidth(), 0.0f, 0.0f);
		gl.glEnd();
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0f, 1f);
			gl.glVertex3f(board.getWidth(), 0.0f, 0.0f);
			gl.glTexCoord2f(0f, 0f);
			gl.glVertex3f(board.getWidth(), 0.0f, -5.0f);
			gl.glTexCoord2f(10f, 0f);
			gl.glVertex3f(board.getWidth(),board.getHeight(), -5.0f);
			gl.glTexCoord2f(10f, 1f);
			gl.glVertex3f(board.getWidth(), board.getHeight(), 0.0f);
		gl.glEnd();
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0f, 1f);
			gl.glVertex3f(board.getWidth(), board.getHeight(), 0.0f);
			gl.glTexCoord2f(0f, 0f);
			gl.glVertex3f(board.getWidth(), board.getHeight(), -5.0f);
			gl.glTexCoord2f(10f, 0f);
			gl.glVertex3f(0.0f, board.getHeight(), -5.0f);
			gl.glTexCoord2f(10f, 1f);
			gl.glVertex3f(0.0f, board.getHeight(), 0.0f);
		gl.glEnd();
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0f, 1f);
			gl.glVertex3f(0.0f, board.getHeight(), 0.0f);
			gl.glTexCoord2f(0f, 0f);
			gl.glVertex3f(0.0f, board.getHeight(), -5.0f);
			gl.glTexCoord2f(10f, 0f);
			gl.glVertex3f(0.0f, 0.0f, -5.0f);
			gl.glTexCoord2f(10f, 1f);
			gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glEnd();
		
		texture_snow.bind(gl);
		gl.glBegin(GL2.GL_TRIANGLE_STRIP);
			float h1 = 0.0f;
			float h2 = 0.0f;
			for(int x = 0; x < board.getWidth(); x++) {
				for(int y = 0; y <=  board.getHeight(); y++) {
					// Srovname kraje
					if ( x != 0 && y != 0 && y != board.getHeight())
						h1 = snowing.getZbuffer()[x][y];
					else
						h1 = 0f;
					if(y + 1 != 1 && y != board.getHeight() && (x + 1 != board.getWidth() ))
						h2 =snowing.getZbuffer()[x + 1][y];
					else
						h2 = 0f;
					gl.glTexCoord2f(0f, 0f);
					gl.glVertex3f(x ,y ,h1 -.1f);
					gl.glTexCoord2f(0f, 1f);
					gl.glVertex3f(x + 1 ,y, h2 -.1f);
				}
			}
		gl.glEnd();
		
		gl.glDisable(GL2.GL_TEXTURE_2D);
			
		// TREE
		gl.glDepthMask(false);
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_ADD);
		
		
		texture_tree.bind(gl);
		
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0f, 0f);
			gl.glVertex3f(board.getWidth()/2-2f, board.getHeight()/2-2f, 0.0f);
			gl.glTexCoord2f(1f, 0f);
			gl.glVertex3f(board.getWidth()/2+2f, board.getHeight()/2+2f, 0.0f);
			gl.glTexCoord2f(1f, 1f);
			gl.glVertex3f(board.getWidth()/2+2f, board.getHeight()/2+2f, 8.0f);
			gl.glTexCoord2f(0f, 1f);
			gl.glVertex3f(board.getWidth()/2-2f, board.getHeight()/2-2f, 8.0f);
		gl.glEnd();
		// DRUHA STRANA
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0f, 0f);
			gl.glVertex3f(board.getWidth()/2-2f, board.getHeight()/2+2f, 0.0f);
			gl.glTexCoord2f(1f, 0f);
			gl.glVertex3f(board.getWidth()/2+2f, board.getHeight()/2-2f, 0.0f);
			gl.glTexCoord2f(1f, 1f);
			gl.glVertex3f(board.getWidth()/2+2f, board.getHeight()/2-2f, 8.0f);
			gl.glTexCoord2f(0f, 1f);
			gl.glVertex3f(board.getWidth()/2-2f, board.getHeight()/2+2f, 8.0f);
		gl.glEnd();
		
//		texture_bush.bind(gl);
//		gl.glBegin(GL2.GL_QUADS);
//			gl.glTexCoord2f(0f, 0f);
//			gl.glVertex3f(board.getWidth()/3-4f, board.getHeight()/3-4f, -.25f);
//			gl.glTexCoord2f(1f, 0f);
//			gl.glVertex3f(board.getWidth()/3-2f, board.getHeight()/3-2f, -.25f);
//			gl.glTexCoord2f(1f, 1f);
//			gl.glVertex3f(board.getWidth()/3-2f, board.getHeight()/3-2f, 3.0f);
//			gl.glTexCoord2f(0f, 1f);
//			gl.glVertex3f(board.getWidth()/3-4f, board.getHeight()/3-4f, 3.0f);
//		gl.glEnd();
//		// DRUHA STRANA
//		gl.glBegin(GL2.GL_QUADS);
//			gl.glTexCoord2f(0f, 0f);
//			gl.glVertex3f(board.getWidth()/3-4f, board.getHeight()/3-2f, -.25f);
//			gl.glTexCoord2f(1f, 0f);
//			gl.glVertex3f(board.getWidth()/3-2f, board.getHeight()/3-4f, -.25f);
//			gl.glTexCoord2f(1f, 1f);
//			gl.glVertex3f(board.getWidth()/3-2f, board.getHeight()/3-4f, 3.0f);
//			gl.glTexCoord2f(0f, 1f);
//			gl.glVertex3f(board.getWidth()/3-4f, board.getHeight()/3-2f, 3.0f);
//		gl.glEnd();
		
		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glDisable(GL2.GL_BLEND);
		
		gl.glDepthMask(true);
		
		if((Integer)controller.getSpinnerCloud().getValue() != tempCloud){
			tempCloud = (Integer)controller.getSpinnerCloud().getValue();
			snowing.getCloud().generateCloud();
		}
		
		gl.glPushMatrix();
		int i = 0, ii = 0;
		for(float w = snowing.getCloud().getX() - (snowing.getCloud().getWidth() / 2); w < snowing.getCloud().getX() + (snowing.getCloud().getWidth() / 2); w++) {
			for(float h = snowing.getCloud().getY() - (snowing.getCloud().getHeight() / 2); h < snowing.getCloud().getY() + (snowing.getCloud().getHeight() / 2); h++) {
				gl.glPushMatrix();
					float color = snowing.getCloud().getCcolor()[i][ii];
					gl.glColor3f(color, color, color);
					gl.glTranslatef(w + .5f , h + .5f, snowing.getCloud().getZ());
					glut.glutSolidSphere(snowing.getCloud().getCbuffer()[i][ii], 10, 10);
				gl.glPopMatrix();
				ii++;
			}
			i++;
			ii = 0;
		}
		gl.glPopMatrix();
		
		gl.glFlush();
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void keyTyped(KeyEvent e) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void keyPressed(KeyEvent e) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void keyReleased(KeyEvent e) {
		float step = .5f;
		switch (e.getKeyCode()){
			case KeyEvent.VK_I:{
				snowing.getCloud().setX(snowing.getCloud().getX() - 0.25f);
				break;
			}
			case KeyEvent.VK_K:{
				snowing.getCloud().setX(snowing.getCloud().getX() + 0.25f);
				break;
			}
			case KeyEvent.VK_J:{
				snowing.getCloud().setY(snowing.getCloud().getY() - 0.25f);
				break;
			}
			case KeyEvent.VK_L:{
				snowing.getCloud().setY(snowing.getCloud().getY() + 0.25f);
				break;
			}
			case KeyEvent.VK_V:{
				wire = !wire;
				break;
			}
			case KeyEvent.VK_W:{
				snowing.getWind().setX(snowing.getWind().getX() - 0.02f);
				break;
			}
			case KeyEvent.VK_S:{
				snowing.getWind().setX(snowing.getWind().getX() + 0.02f);
				break;
			}
			case KeyEvent.VK_D:{
				snowing.getWind().setY(snowing.getWind().getY() + 0.02f);
				break;
			}
			case KeyEvent.VK_A:{
				snowing.getWind().setY(snowing.getWind().getY() - 0.02f);
				break;
			}
			case KeyEvent.VK_U:{
				if(snowing.getCloud().getZ() - .25f > 8f){
					snowing.getCloud().setZ(snowing.getCloud().getZ() - .25f);
				}
				break;
			}
			case KeyEvent.VK_O:{
				snowing.getCloud().setZ(snowing.getCloud().getZ() + .25f);
				break;
			}
			case KeyEvent.VK_R:{
				snowing.removeSnowFlakes();
				snowing.removeOldSnowFlake();
				snowing.resetWind();
				snowing.start();
				break;
			}
			case KeyEvent.VK_T:{
				axis = !axis;
				break;
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			startX = e.getX();
			startY = e.getY();
		}
		if (SwingUtilities.isRightMouseButton(e)) {
			windX = e.getX();
			windY = e.getY();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void mouseEntered(MouseEvent e) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void mouseExited(MouseEvent e) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			float step = .25f;
			dx = e.getX() - startX;
			dy = e.getY() - startY;
			startX = e.getX();
			startY = e.getY();
		}
		if (SwingUtilities.isRightMouseButton(e)) {
			float wx = e.getX() - windX, wy = e.getY() - windY;
			//snowing.setWind(wx);
			snowing.getWind().setX(snowing.getWind().getX() + (wx/20));
			snowing.getWind().setY(snowing.getWind().getY() + (wy/20));
			windX = e.getX();
			windY = e.getY();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
//		throw new UnsupportedOperationException("Not supported yet."); //To change bopy of generated methods, choose Tools | Templates.
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if(e.getWheelRotation() < 0){
			if(zoom + 1 < -board.getFurther()*.75){
				zoom++;
			}
		}else{
			if(zoom - 1 > -board.getFurther()* 3){
				zoom--;
			}
		}
	}
}
