/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasterization;

import grafika.Controller;
import java.util.Random;

/**
 *
 * @author lukas.richtrmoc
 */
public class Cloud {
	private Controller controller;
	private float x, y, width, height;
	private float z;
	
	private float[][] cbuffer, ccolor;

	public Cloud(Board board, Controller controller) {
		this.controller = controller;
		
		x = board.getWidth() / 2;
		y = board.getHeight() / 2;
		z = board.getWidth();
		
		generateCloud();
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}
	
	public void generateCloud(){
		setCloud();
		Random r = new Random();
		float halfX = width / 2, halfY = height / 2;
		
		// 1 - ochrana pred delenim nulou
		for (float w = 1; width >= w; w++){
			for (float h = 1; height >= h; h++){
				float sizeX, sizeY;
				if (w > halfX ){
					sizeX = halfX / w;
				}else{
					sizeX = w / halfX;
				}
				if (h > halfY ){
					sizeY = halfY / h;
				}else{
					sizeY = h / halfY;
				}
				float size = 5 * (sizeX * sizeY * .8f);
				if( size > 4 ){
					size *= .65f;
				}
				if( size < 1){
					size = 0;
				}
				
				cbuffer[Math.round(w)-1][Math.round(h)-1] = 0 + r.nextFloat() * size;
				ccolor[Math.round(w)-1][Math.round(h)-1] = 0.40f + r.nextFloat() * 0.45f;
			}
		}
	}
	
	private void setCloud(){
		width = (Integer)controller.getSpinnerCloud().getValue();
		height = Math.round((Integer)controller.getSpinnerCloud().getValue() * .7f);
		cbuffer = new float[Math.round(width)+1][Math.round(height)+1];
		ccolor = new float[Math.round(width)+1][Math.round(height)+1];
	}

	public float[][] getCbuffer() {
		return cbuffer;
	}

	public float[][] getCcolor() {
		return ccolor;
	}
	
}
