/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasterization;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import grafika.Controller;


/**
 *
 * @author lukas.richtrmoc
 */
public class Snowing {
	private Controller controller;
	private List<SnowFlake> elements;
	private Wind wind;
	private Cloud cloud;
	private float fallingSpeed, timer;
	private int fallingQuantity, borderWidth, borderHeight, temperature;
	private float[][] zbuffer;
	private boolean falling, snowing;
	private Random r;

	public Snowing(Board board, Controller controller) {
		this.controller = controller;
		this.elements = new ArrayList<>();
		this.wind = new Wind();
		this.cloud = new Cloud(board, controller);
		this.fallingSpeed = controller.getSliderSpeed().getValue();
		this.fallingQuantity = controller.getSliderQuantity().getValue();
		this.timer = 0;
		this.borderHeight = board.getWidth();
		this.borderWidth = board.getHeight();
		this.r = new Random();
		this.snowing = true;
		this.falling = true;
		this.zbuffer = new float[board.getWidth()+1][board.getHeight()+1];
	}
	
	public void refreshFlakes(){
		fallingSpeed = controller.getSliderSpeed().getValue() / 50f;
		if (falling && fallingSpeed > 0){
			for(int i = 0; elements.size() > i ; i++){
				SnowFlake flake = elements.get(i);
				if (flake.getZ() > flake.getSize() + fallingSpeed){
					flake.setZ(flake.getZ() - fallingSpeed);
					
					if(flake.getX() + wind.getX() > 0 && flake.getX() + wind.getX()  < borderWidth && flake.getY() + wind.getY() > 0 && flake.getY() + wind.getY() < borderHeight){
						flake.setX(flake.getX() + wind.getX());
						flake.setY(flake.getY() + wind.getY());
					}else{
						elements.remove(i);
						i--;
					}
				}else{
					if (zbuffer[Math.round(flake.getX())][Math.round(flake.getY())] + flake.getSize() >= cloud.getZ()){
						stop();
					}else{
						zbuffer[Math.round(flake.getX())][Math.round(flake.getY())] += (flake.getSize() - temperature / 10f)/ 100;//* (.5f + Math.pow(temperature, 1.05f));
						elements.remove(i);
						i--;
					}
				}
			}
		}
		if (snowing && fallingSpeed > 0){
			addSnowFlake();
		}
		temperature = (Integer)controller.getSliderTemperature().getValue();
		if(temperature > 0){
			float temparutureC = temperature * 3 / 10000.f;
			for (int x = 0; x < zbuffer.length; x++){
				for(int y = 0; y < zbuffer.length; y++){
					if (zbuffer[x][y] - temparutureC > -0.1f){
						zbuffer[x][y]  -= temparutureC; 
					}else{
						if (zbuffer[x][y] != -0.1f){
							zbuffer[x][y]  = -0.1f;
						}
					}
				}
			}
		}
	}
	
	private void addSnowFlake(){
		fallingQuantity = controller.getSliderQuantity().getValue();
		for (int i = 0; i < fallingQuantity; i++){
			int x = 0 + Math.round(r.nextFloat() * cloud.getWidth());
			int y = 0 + Math.round(r.nextFloat() * cloud.getHeight());
			
			if (cloud.getCbuffer()[x][y] > 0){
				float xx = ( cloud.getX() + (cloud.getWidth()/2) - x);
				float yy = ( cloud.getY() + (cloud.getHeight()/2) - y);
			
				elements.add(new SnowFlake(xx, yy, cloud.getZ(), 0 + r.nextFloat() * 3));
			}
		}
	}
	
	public void removeOldSnowFlake(){
		for (int w = 0; w < borderWidth; w++) {
			for (int h = 0; h < borderHeight; h++) {
				zbuffer[w][h] = 0;
			}
		}
	}
	
	public void removeSnowFlakes(){
		elements.clear();
	}

	public boolean isRunning() {
		return snowing && falling;
	}

	public void start() {
		this.snowing = true;
		this.falling = true;
	}
	
	public void stop() {
		this.snowing = false;
		this.falling = true;
	}

	public List<SnowFlake> getFlakes() {
		return elements;
	}

	public float[][] getZbuffer() {
		return zbuffer;
	}

	public boolean isSnowing() {
		return snowing;
	}

	public void setSnowing(boolean snowing) {
		this.snowing = snowing;
	}

	public float getFallingSpeed() {
		return fallingSpeed;
	}

	public void speedUp(){
		fallingSpeed *= 1.2f;
	}
	
	public void speedDown(){
		fallingSpeed *= .8f;
	}
	
	public void quantityUp() {
		if(fallingQuantity < 10){
			fallingQuantity++;
		}
	}

	public void quantityDown() {
		if(fallingQuantity > 0){
			fallingQuantity--;
		}
	}

	public Wind getWind() {
		return wind;
	}
	
	public void resetWind(){
		wind = new Wind();
	}

	public Cloud getCloud() {
		return cloud;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}
}
