/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasterization;

/**
 *
 * @author lukas.richtrmoc
 */
public class Board {

	private int x, y, width, height;
	
	public Board(int width, int height) {
		x = 0;
		y = 0;
		this.width = width;
		this.height = height;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public int getFurther(){
		if(height > width)
			return height;
		else
			return width;
	}
}
